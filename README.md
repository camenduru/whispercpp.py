Python bindings for whisper.cpp
===============================

```
git clone --recurse-submodules https://git.ecker.tech/lightmare/whispercpp.py
cd whispercpp.py
pip install .
```
or
```
git clone https://git.ecker.tech/lightmare/whispercpp.py
cd whispercpp.py
git submodule update --init
pip install .
```

```python
from whispercpp import Whisper

w = Whisper('tiny', models_dir='./models/', language=b'en')

result = w.transcribe("myfile.mp3")
text = w.extract_text(result)
```

Note: default parameters might need to be tweaked.  
See Whispercpp.pyx.

Changes made (not exhaustive):
- changed defaults
- whisper.cpp submodule set to 1.2.0
- removed `requests` dependency
- models dir can be changed in constructor
- added support for setting params
- added back support for `large-v1` model
- added support for english-only models
